from django.conf.urls import patterns, include, url
from django.conf import settings


urlpatterns = patterns(
    '',
    url(r'^register/?$', 'user_auth.views.register'),
    url(r'^login', 'user_auth.views.obtain_auth_token'),
    url(r'^logout', 'user_auth.views.logout'),
    url(r'^change-password', 'user_auth.views.change_password'),
    url(r'^forgot-password', 'user_auth.views.forgot_password'),
    url(r'^reset-password', 'user_auth.views.reset_password'),
)
