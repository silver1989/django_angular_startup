from django.contrib import admin

from .models import UserProfile, Book


admin.site.register(UserProfile, admin.ModelAdmin)
admin.site.register(Book, admin.ModelAdmin)