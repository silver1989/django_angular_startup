from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


class UserProfile(models.Model):
    user = models.OneToOneField(User, null=False, blank=None)
    birthday = models.DateField(null=True, blank=None)


class Book(models.Model):
    title = models.CharField(max_length=255, null=False, blank=None)
    user = models.ForeignKey(User, null=False, blank=None)

    def __unicode__(self):
        return u'%s' % self.title

@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.get_or_create(user=instance)
        instance.save()