from django.contrib.auth import authenticate

from rest_framework.authtoken.models import Token
from rest_framework import serializers

from django.contrib.auth import get_user_model
User = get_user_model()


class CreateUserSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(write_only=True)
    birthday = serializers.DateField(write_only=True)

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'password', 'password2', 'birthday')
        write_only_fields = ('password', )

    def validate(self, data):
        password1 = data['password']
        password2 = data['password2']
        if password1 and password2 and password1 != password2:
            raise serializers.ValidationError('The two passwords do not match.')
        return data


class PasswordSerializer(serializers.Serializer):
    password = serializers.CharField(
        style={'base_template': 'password.html'},
        required=False
    )


class EmailSerializer(serializers.Serializer):
    email = serializers.EmailField()


class ReturnUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'id')


class LogoutSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email',)

    def restore_object(self, attrs, instance=None):
        email = attrs.pop('email', None)
        #obj = super(LogoutSerializer, self).restore_object(attrs, instance)
        obj = User.objects.get(email=email)
        return obj


class ChangePasswordSerializer(serializers.Serializer):
    token = serializers.CharField()
    current_password = serializers.CharField(
        style={'base_template': 'password.html'},
        required=False
    )
    password = serializers.CharField(
        style={'base_template': 'password.html'},
        required=False
    )
    password2 = serializers.CharField(
        style={'base_template': 'password.html'},
        required=False
    )

    def validate_token(self, attrs, source):
        token = attrs[source]
        token_array = Token.objects.filter(key = token)
        if token_array:
            return attrs
        else:
            raise serializers.ValidationError('User does not exist')

    def validate_password(self, attrs):
        password = attrs['password']
        password_length = 8
        if len(password) < password_length:
          raise serializers.ValidationError('Password must be at least ' + str(password_length) + ' characters')
        return attrs

    def validate_password2(self, attrs, source):
        password2 = attrs.pop(source)
        if attrs['password'] != password2:
            raise serializers.ValidationError('Both passwords must match')
        return attrs


class ForgotPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate_email(self, attrs, source):
        email = attrs[source]
        user_array = User.objects.filter(email = email)
        if user_array:
            return attrs
        else:
            raise serializers.ValidationError('User does not exist')


class ResetPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField()
    change_password_token = serializers.CharField(
        style={'base_template': 'password.html'},
        required=False
    )
    password = serializers.CharField(
        style={'base_template': 'password.html'},
        required=False
    )
    password2 = serializers.CharField(
        style={'base_template': 'password.html'},
        required=False
    )

    def validate_email(self, attrs, source):
        email = attrs[source]
        user_array = User.objects.filter(email=email)
        if user_array:
            return attrs
        else:
            raise serializers.ValidationError('User does not exist')

    def validate_password(self, attrs, source):
        password = attrs['password']
        password_length = 8
        if len(password) < password_length:
          raise serializers.ValidationError('Password must be at least ' + str(password_length) + ' characters')
        return attrs

    def validate_password2(self, attrs, source):
        password2 = attrs.pop(source)
        if attrs['password'] != password2:
            raise serializers.ValidationError('Both passwords must match')
        return attrs


class AuthTokenSerializer(serializers.Serializer):
    """
    Restfuls AuthTokenSerializer with modifications to use email rather than user
    """
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')

        if username and password:
            user = authenticate(username=username, password=password)
            if user:
                if not user.is_active:
                    raise serializers.ValidationError('User account is disabled.')
                attrs['user'] = user
                return attrs
            else:
                raise serializers.ValidationError('Unable to login with provided credentials.')
        else:
            raise serializers.ValidationError('Must include "username" and "password"')