App.service('appService', ['$http', '$cookies', '$resource', 'localStorageService',
    function($http, $cookies, $resource, localStorageService){
        var urlBase = "http://127.0.0.1:8000/";
        this.signin = function(user_info, cbSuccess, cbFailure) {
            var AuthToken = $resource(urlBase + "accounts/login/", user_info);
            var authToken = AuthToken.save(user_info, function () {
                localStorageService.add('Authorization', 'Token ' + authToken.token);
                localStorageService.add('rest_token', authToken.token);
                localStorageService.add('user_id', authToken.id);
                cbSuccess();
            }, function (error) {
                cbFailure(error);
            });
        }
     }]);