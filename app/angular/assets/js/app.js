'use strict';

window.App = angular.module('angularApp',
    [
        'ngRoute', 'ngResource', 'ngCookies', 'ngResource',
        'LocalStorageModule', 'validation', 'validation.rule'
    ]);

App.config(['$routeProvider', '$httpProvider',
    function($routeProvider, $httpProvider) {
        function checkLogin(authService) {
            authService.checkAuth();
        }
        $routeProvider.
            when('/sign', {
                templateUrl: 'assets/js/views/signin.html',
                controller: 'signinCtrl'
            }).
            when('/profile', {
                templateUrl: 'assets/js/views/profile.html',
                controller: 'profileCtrl',
                resolve: {
                    load: checkLogin
                }
            }).

            when('/register', {
                templateUrl: 'assets/js/views/register.html',
                controller: 'registrationCtrl'
            }).
            otherwise({
                redirectTo: '/profile'
            });
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
        $httpProvider.defaults.withCredentials = true;

        // Set the httpProvider "not authorized" interceptor
        $httpProvider.interceptors.push(['$q', '$location',
            function ($q, $location) {
                return {
                    responseError: function (rejection) {
                        switch (rejection.status) {
                            case 401:
                                // Redirect to signin page
                                $location.path('signin');
                                break;
                            case 403:
                                // Add unauthorized behaviour
                                break;
                        }

                        return $q.reject(rejection);
                    }
                };
            }
        ]);
    }
    ]).run(function($rootScope, $location){
	  $rootScope.checkUserLoggedIn = function () {
			// if user not logged in then go to login page
			if ($rootScope.access_token == undefined || $rootScope.access_token == '') {
				$location.path("/signin");
			}
			return true;
		};
    $rootScope.$on("notAuthenticated", function () {
        $location.path("/sign");
    });
  });

App.factory('authService', ['$rootScope', '$q', 'localStorageService', '$http', '$timeout',
    function($rootScope, $q, localStorageService, $http, $timeout){
        return {
            checkAuth: function () {
                $rootScope.token = localStorageService.get('Authorization');
                $http.defaults.headers.common['Authorization'] = localStorageService.get('Authorization');

                var defer = $q.defer();

                if($rootScope.token){
                    $timeout(function () {
                        defer.resolve();
                    });
                } else {
                    $timeout(function () {
                        defer.reject("not_logged_in");
                    });
                    $rootScope.$broadcast('notAuthenticated');
                }
                return defer.promise;
            }
        }

    }]);