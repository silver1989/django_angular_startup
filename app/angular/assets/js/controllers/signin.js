'use strict';

App.controller('signinCtrl', function ($scope, $rootScope, $location, appService ) {

    $scope.username = "";
    $scope.password = "";
    $scope.user_remember = true;

    $scope.login = function(){
        var user = {
            username: $scope.username,
            password: $scope.password,
            grant_type: "password"
        };
        appService.signin(user, function() {
            $location.path("/profile");
        }, function (error) {
            $scope.message = error.data;
            //console.log("Authentication failed.");
        });
    }

});

