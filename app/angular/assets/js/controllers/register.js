'use strict';

App.controller('registrationCtrl',
    function(localStorageService, $resource, $http, $scope, $location) {
        $scope.restProtocol = 'http';
        $scope.restURL = '127.0.0.1:8000';
        $scope.user = {
            first_name: '',
            last_name: '',
            email: '',
            password: '',
            password2: '',
            birthday:''
        };
        var AuthToken =  $resource(":protocol://:url/accounts/register/", {
            protocol: $scope.restProtocol,
            url: $scope.restURL
        });
        $scope.submit = function(){
            // AutoFill Fix
            // angular.element(document.getElementsByTagName('input')).checkAndTriggerAutoFillEvent();

            $scope.authToken = AuthToken.save($scope.user, function(){
                localStorageService.add('Authorization', 'Token ' + $scope.authToken.token);
                localStorageService.add('rest_token', $scope.authToken.token);
                localStorageService.add('user_id', $scope.authToken.id);
                localStorageService.add('user_email', $scope.authToken.email);
                localStorageService.add('user_img', $scope.authToken.img);
                localStorageService.add('user_type', $scope.authToken.type);
                $location.path("/profile");
            },function(error) {
                $scope.message = error.data;
            });
        };

});
